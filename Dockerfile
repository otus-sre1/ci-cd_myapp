FROM python:3.9
RUN pip install --no-cache-dir Flask==2.3.2 uWSGI==2.0.21
WORKDIR /app
COPY app /app
EXPOSE 5000
CMD ["python", "myapp.py"]
