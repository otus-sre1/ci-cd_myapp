# Описание выполнения работы

При выполнении работы использовалось [простое python приложение](https://gitlab.com/otus-sre1/ci-cd_myapp/-/blob/develop/app/myapp.py).

Дефолтным установлен бранч develop  

Pipeline состоит:

1. Из [линтеров](https://gitlab.com/otus-sre1/ci-cd_myapp/-/tree/develop/lint) (docker-lint, python-lint, yamllint), запускаемых в случае изменения кода.
1. [Сборки docker образа](https://gitlab.com/otus-sre1/ci-cd_myapp/-/blob/develop/.gitlab/ci/build.yaml), в случае успешного прохождения линтеров.  
2.1. [Pipeline упал](https://gitlab.com/otus-sre1/ci-cd_myapp/-/pipelines/900736266) из-за ошибки в работе линтера. Сборка образа прервана.  
2.2. Внесены исправления. [Pipeline отработал успешно](https://gitlab.com/otus-sre1/ci-cd_myapp/-/pipelines/900751399), docker образ запушен.
